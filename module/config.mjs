export const CFG = {
	module: 'koboldworks-pause-control',
	COLORS: {
		main: 'color:orangered',
		label: 'color:mediumseagreen',
		unset: 'color:unset',
		number: 'color:mediumpurple',
	}
};
